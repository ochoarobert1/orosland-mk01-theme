<?php

//function portafolio() {
//
//    $labels = array(
//        'name'                  => _x( 'Portafolios', 'Post Type General Name', 'orosland' ),
//        'singular_name'         => _x( 'Portafolio', 'Post Type Singular Name', 'orosland' ),
//        'menu_name'             => __( 'Portafolio', 'orosland' ),
//        'name_admin_bar'        => __( 'Portafolio', 'orosland' ),
//        'archives'              => __( 'Archivo de Portafolio', 'orosland' ),
//        'attributes'            => __( 'Atributos de Portafolio', 'orosland' ),
//        'parent_item_colon'     => __( 'Portafolio Padre:', 'orosland' ),
//        'all_items'             => __( 'Todos los Items', 'orosland' ),
//        'add_new_item'          => __( 'Agregar Nuevo Item', 'orosland' ),
//        'add_new'               => __( 'Agregar Nuevo', 'orosland' ),
//        'new_item'              => __( 'Nuevo Item', 'orosland' ),
//        'edit_item'             => __( 'Editar Item', 'orosland' ),
//        'update_item'           => __( 'Actualizar Item', 'orosland' ),
//        'view_item'             => __( 'Ver Item', 'orosland' ),
//        'view_items'            => __( 'Ver Portafolio', 'orosland' ),
//        'search_items'          => __( 'Buscar en Portafolio', 'orosland' ),
//        'not_found'             => __( 'No hay Resultados', 'orosland' ),
//        'not_found_in_trash'    => __( 'No hay Resultados en la Papelera', 'orosland' ),
//        'featured_image'        => __( 'Imagen Destacada', 'orosland' ),
//        'set_featured_image'    => __( 'Colocar Imagen Destacada', 'orosland' ),
//        'remove_featured_image' => __( 'Remover Imagen Destacada', 'orosland' ),
//        'use_featured_image'    => __( 'Usar como Imagen Destacada', 'orosland' ),
//        'insert_into_item'      => __( 'Insertar dentro de Item', 'orosland' ),
//        'uploaded_to_this_item' => __( 'Cargado a este item', 'orosland' ),
//        'items_list'            => __( 'Listado del Portafolio', 'orosland' ),
//        'items_list_navigation' => __( 'Navegación de Listado del Portafolio', 'orosland' ),
//        'filter_items_list'     => __( 'Filtro de Listado del Portafolio', 'orosland' ),
//    );
//    $args = array(
//        'label'                 => __( 'Portafolio', 'orosland' ),
//        'description'           => __( 'Portafolio de Desarrollos', 'orosland' ),
//        'labels'                => $labels,
//        'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', 'comments', 'trackbacks', 'custom-fields', ),
//        'taxonomies'            => array( 'custom_portafolio' ),
//        'hierarchical'          => false,
//        'public'                => true,
//        'show_ui'               => true,
//        'show_in_menu'          => true,
//        'menu_position'         => 5,
//        'menu_icon'             => 'dashicons-testimonial',
//        'show_in_admin_bar'     => true,
//        'show_in_nav_menus'     => true,
//        'can_export'            => true,
//        'has_archive'           => true,
//        'exclude_from_search'   => false,
//        'publicly_queryable'    => true,
//        'capability_type'       => 'post',
//        'show_in_rest'          => true,
//    );
//    register_post_type( 'portafolio', $args );
//
//}
//add_action( 'init', 'portafolio', 0 );

