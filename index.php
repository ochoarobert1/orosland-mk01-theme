<?php get_header(); ?>
<main class="container" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <section class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
            <?php $defaultatts = array('class' => 'img-responsive', 'itemprop' => 'image'); ?>
            <?php $args = array('post_type' => 'post', 'posts_per_page' => 1, 'order' => 'DESC', 'orderby' => 'date'); ?>
            <?php query_posts($args); ?>
            <?php while (have_posts()) : the_post(); ?>
            <article id="post-<?php echo get_the_ID(); ?>" class="blog-item-featured col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo join(' ', get_post_class()); ?>" role="article">
                <picture>
                    <?php the_post_thumbnail('blog_img_featured', $defaultatts); ?>
                </picture>
                <header class="blog-item-featured-wrapper">
                    <div class="post-category">
                        <?php the_category(' '); ?>
                    </div>
                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                        <h2 rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></h2>
                    </a>
                    <meta itemprop="datePublished" datetime="<?php echo get_the_time('Y-m-d') ?>" content="<?php echo get_the_date('i') ?>">
                    <meta itemprop="author" content="<?php echo esc_attr(get_the_author()) ?>">
                    <meta itemprop="url" content="<?php the_permalink() ?>">
                </header>
            </article>
            <?php endwhile; ?>
            <?php wp_reset_query(); ?>
            <div class="clearfix"></div>
            <h2 class="blog-divider"><?php _e('Otras Entradas...', 'orosland'); ?></h2>
            <?php query_posts('offset=1'); ?>
            <?php $i = 1; ?>
            <div class="blog-main-loop col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                <article id="post-<?php echo get_the_ID(); ?>" class="blog-item col-lg-4 col-md-4 col-sm-4 col-xs-12 no-paddingl no-paddingr <?php echo join(' ', get_post_class()); ?>" role="article">

                    <div class="blog-item-container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <picture>
                            <?php the_post_thumbnail('blog_img', $defaultatts); ?>
                        </picture>
                        <header class="blog-item-wrapper">
                            <div class="post-category">
                                <?php the_category(' '); ?>
                            </div>
                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                <h2 rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></h2>
                            </a>
                            <meta itemprop="datePublished" datetime="<?php echo get_the_time('Y-m-d') ?>" content="<?php echo get_the_date('i') ?>">
                            <meta itemprop="author" content="<?php echo esc_attr(get_the_author()) ?>">
                            <meta itemprop="url" content="<?php the_permalink() ?>">
                        </header>
                    </div>
                </article>
                <?php endwhile; ?>
            </div>
            <div class="pagination col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php if(function_exists('wp_paginate')) { wp_paginate(); } else { posts_nav_link(); wp_link_pages(); } ?>
            </div>
            <?php else: ?>
            <article class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h2><?php _e('Disculpe, su busqueda no arrojo ningun resultado', 'orosland'); ?></h2>
                <h3><?php _e('Dirígete nuevamente al', 'orosland'); ?> <a href="<?php echo home_url('/'); ?>" title="<?php _e('Volver al Inicio', 'orosland'); ?>"><?php _e('inicio', 'orosland'); ?></a>.</h3>
            </article>
            <?php endif; ?>
        </section>
    </div>
</main>
<?php get_footer(); ?>
