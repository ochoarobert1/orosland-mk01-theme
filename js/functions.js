jQuery(document).ready(function ($) {
    "use strict";
    jQuery("#sticker").sticky({
        topSpacing: 0
    });
    jQuery("body").niceScroll();
    jQuery('.lettering-title').lettering('words');
}); /* end of as page load scripts */
