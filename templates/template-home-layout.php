<?php

/**
 * Template Name: Homepage Template Layout
 *
 * @package orosland-mk01-theme
 * @subpackage templates
 * @since Orosland 1.0
 */

?>
<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <section class="the-slider col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <?php $slider = get_post_meta(get_the_ID(), 'rw_slider', true); ?>
            <?php echo do_shortcode('[rev_slider alias="' . $slider . '"]'); ?>
        </section>
        <?php $args = array('post_type' => 'product', 'posts_per_page' => 6, 'order' => 'DESC', 'orderby' => 'date', 'tax_query' => array( array( 'taxonomy' => 'product_visibility', 'field'    => 'name', 'terms'    => 'featured', 'operator' => 'IN' ))); ?>
        <?php query_posts($args); ?>
        <?php if (have_posts()) : ?>
        <section class="the-home-shop col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="home-shop-container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h2 class="section-title"><?php _e('Store', 'orosland'); ?></h2>
                        <?php while (have_posts()) : the_post(); ?>
                        <article class="home-shop-item col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="home-shop-item-wrapper col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <picture>
                                    <a href="<?php the_permalink(); ?>" title="<?php echo get_the_title(); ?>">
                                        <?php if (has_post_thumbnail()) { ?>
                                        <?php the_post_thumbnail('home_shop_img', array('class' => 'img-responsive', 'itemprop' => 'image')); ?>
                                        <?php } else { ?>
                                        <img src="http://placehold.it/200x200" alt="<?php echo get_the_title(); ?>" class="img-responsive" />
                                        <?php } ?>
                                    </a>
                                </picture>
                                <h3><?php the_title(); ?></h3>
                            </div>
                        </article>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>

        </section>
        <?php endif; ?>
        <?php wp_reset_query(); ?>
    </div>
</main>
<?php get_footer(); ?>
