<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>
<?php $shop_id = get_page_by_title('store'); ?>
<?php $slider = get_post_meta($shop_id->ID, 'rw_slider', true); ?>



<?php if (!$slider == '') { ?>
<section class="the-slider col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
    <?php echo do_shortcode('[rev_slider alias="' . $slider . '"]'); ?>
</section>
<?php } ?>

<?php
/**
         * woocommerce_before_main_content hook.
         *
         * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
         * @hooked woocommerce_breadcrumb - 20
         * @hooked WC_Structured_Data::generate_website_data() - 30
         */
do_action( 'woocommerce_before_main_content' );
?>
<h2 class="custom-shop-headline-title">
    <?php _e('Categorias', 'orosland'); ?>
</h2>

<section class="custom-shop-categories-container col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">

    <?php $terms = get_terms( array( 'taxonomy' => 'product_cat', 'hide_empty' => false, ) ); ?>

    <?php if ( $terms && ! is_wp_error( $terms ) ) : ?>

    <?php foreach ( $terms as $term ) { ?>
    <div class="custom-shop-categories-item col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <picture>
            <?php $thumbnail_id = get_woocommerce_term_meta( $term->term_id, 'thumbnail_id', true ); ?>

            <?php $image = wp_get_attachment_image_src( $thumbnail_id, 'blog_img' ); ?>

            <img src="<?php echo $image[0]; ?>" alt="<?php echo $term->name; ?>" class="img-responsive" />
        </picture>
        <a href="<?php echo get_term_link($term, 'product_cat'); ?>" title="<?php echo $term->name; ?>">
            <div class="custom-shop-categories-item-wrapper col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <header class="categories-item-wrapper">
                    <h3>
                        <?php echo $term->name; ?>
                    </h3>
                </header>
            </div>
        </a>
    </div>
    <?php } ?>

    <?php endif; ?>

</section>

<?php
/**
         * woocommerce_after_main_content hook.
         *
         * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
         */
do_action( 'woocommerce_after_main_content' );
?>

<?php get_footer( 'shop' ); ?>
