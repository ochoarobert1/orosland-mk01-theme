<?php

/**
 * Template Name: Contact Template Layout
 *
 * @package orosland-mk01-theme
 * @subpackage templates
 * @since Orosland 1.0
 */

?>
<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <section class="the-contact col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="contact-img col-lg-6 col-md-6 col-sm-6 col-xs-12 no-paddingl no-paddingr">
                <?php the_post_thumbnail('full', array('class' => 'img-responsive animated fadeIn', 'itemprop' => 'image')); ?>
            </div>
            <div class="contact-info col-lg-6 col-md-6 col-sm-6 col-xs-12 no-paddingl no-paddingr">
                <div class="contact-info-wrapper col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                    <h1 class="lettering-title"><?php _e('Contact Us', 'orosland'); ?></h1>
                    <?php the_content(); ?>
                </div>
            </div>

        </section>
    </div>
</main>
<?php get_footer(); ?>
