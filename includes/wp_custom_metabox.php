<?php
/* --------------------------------------------------------------
    GET CUSTOM REVSLIDER
-------------------------------------------------------------- */

function get_custom_slider() {
    global $wpdb;
    $the_query = "SELECT title, alias FROM " . $wpdb->prefix . "revslider_sliders";
    $sliders = $wpdb->get_results($the_query, 'ARRAY_A');
    $item_container = [];
    $i = 1;
    if (empty($sliders)) {
        $item_container[] = array(" " => " ");
    } else {
        foreach ($sliders as $item){
            $itemkeys[] = $item['alias'];
            $itemvalues[] = $item['title'];
        }
        $item_container[] = array_combine($itemkeys, $itemvalues);
    }
    return $item_container[0];
}


add_filter( 'rwmb_meta_boxes', 'orosland_metabox' );

function orosland_metabox( $meta_boxes )
{
    $prefix = 'rw_';

    $meta_boxes[] = array(
        'id'         => 'home_data',
        'title'      => __( 'Secciones de la Pagina / Información Extra', 'orosland' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'include' => array(
            // With all conditions below, use this logical operator to combine them. Default is 'OR'. Case insensitive. Optional.
            'relation'        => 'OR',
            'slug'            => array( 'home', 'inicio' ),
            'template'        => array(  'templates-home-layout.php' ),
        ),
        'fields' => array(
            array(
                'id'   => 'custom_html',
                'type' => 'custom_html',
                'std'  => '<div class="custom-metabox-headline"><h2>' . __('Slider Principal', 'orosland'). '</h2></div>',
                // 'callback' => 'display_warning',
            ),
            array(
                'name'     => __( 'Seleccione Revolution Slider', 'orosland' ),
                'id'       => $prefix . 'slider',
                'type'     => 'select_advanced',
                // Array of 'value' => 'Label' pairs for select box
                'options'  => get_custom_slider(),
                // Select multiple values, optional. Default is false.
                'multiple' => false,
                'std'         => ' ', // Default value, optional
                'placeholder' => __( 'Seleccione un slider', 'orosland' ),
            )
        )
    );

    /* METABOX FOR STORE PAGE */
    $meta_boxes[] = array(
        'id'         => 'shop_data',
        'title'      => __( 'Secciones de la Pagina / Información Extra', 'orosland' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'include' => array(
            // With all conditions below, use this logical operator to combine them. Default is 'OR'. Case insensitive. Optional.
            'relation'        => 'OR',
            'slug'            => array( 'store', 'shop', 'tienda' ),
        ),
        'fields' => array(
            array(
                'id'   => 'custom_html',
                'type' => 'custom_html',
                'std'  => '<div class="custom-metabox-headline"><h2>' . __('Slider Principal', 'orosland'). '</h2></div>',
                // 'callback' => 'display_warning',
            ),
            array(
                'name'     => __( 'Seleccione Revolution Slider', 'orosland' ),
                'id'       => $prefix . 'slider',
                'type'     => 'select_advanced',
                // Array of 'value' => 'Label' pairs for select box
                'options'  => get_custom_slider(),
                // Select multiple values, optional. Default is false.
                'multiple' => false,
                'std'         => ' ', // Default value, optional
                'placeholder' => __( 'Seleccione un slider', 'orosland' ),
            )
        )
    );

    return $meta_boxes;
}
?>
