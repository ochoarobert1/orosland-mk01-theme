<?php
/* WOOCOMMERCE CUSTOM COMMANDS */

/* WOOCOMMERCE - DECLARE THEME SUPPORT - BEGIN */
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
    add_theme_support( 'wc-product-gallery-zoom' );
    add_theme_support( 'wc-product-gallery-lightbox' );
    add_theme_support( 'wc-product-gallery-slider' );
}

/* WOOCOMMERCE - DECLARE THEME SUPPORT - END */

/* WOOCOMMERCE - CUSTOM WRAPPER - BEGIN */
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

add_action('woocommerce_before_main_content', 'my_theme_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'my_theme_wrapper_end', 10);

function my_theme_wrapper_start() {
    echo '<section id="main" class="container"><div class="row"><div class="woocustom-main-container col-lg-12 col-md-12 col-sm-12 col-xs-12">';
}

function my_theme_wrapper_end() {
    echo '</div></div></section>';
}
/* WOOCOMMERCE - CUSTOM WRAPPER - END */

add_filter( 'woocommerce_product_add_to_cart_text', 'woo_archive_custom_cart_button_text' );

function woo_archive_custom_cart_button_text() {

    return __( 'View More', 'orosland' );

}

remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);

// Change number or products per row to 3
add_filter('loop_shop_columns', 'loop_columns');
if (!function_exists('loop_columns')) {
    function loop_columns() {
        return 3;
    }
}

add_action('woocommerce_after_shop_loop_item_title', 'product_custom_links', 15);

function product_custom_links() {
?>
<div class="product-custom-links">
    <a href="<?php echo get_permalink() . '?add_to_wishlist=' . get_the_ID(); ?>" class="product-heart"><i class="fa fa-heart"></i></a>
    <a href="<?php echo get_permalink() . '?add-to-cart=' . get_the_ID(); ?>" class="product-cart"><i class="fa fa-shopping-cart"></i></a>
</div>
<?php
                                }

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15);
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
add_action('woocommerce_single_product_summary', 'product_custom_links_single', 40);


function product_custom_links_single() {
?>
<div class="product-custom-links product-custom-links-single">
    <a href="<?php echo get_permalink() . '?add_to_wishlist=' . get_the_ID(); ?>" class="product-heart"><i class="fa fa-heart"></i></a>
    <a href="<?php echo get_permalink() . '?add-to-cart=' . get_the_ID(); ?>" class="product-cart"><i class="fa fa-shopping-cart"></i></a>
</div>
<?php
                                       }
