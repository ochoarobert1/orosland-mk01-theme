<?php
/** FUNCION PARA COLOCAR TIEMPO EN ENTRADAS **/
function orosland_time_ago() {
    global $post;
    $date = get_post_time('G', true, $post);
    $chunks = array(
        array( 60 * 60 * 24 * 365 , __( 'año', 'orosland' ), __( 'años', 'orosland' ) ),
        array( 60 * 60 * 24 * 30 , __( 'mes', 'orosland' ), __( 'meses', 'orosland' ) ),
        array( 60 * 60 * 24 * 7, __( 'semana', 'orosland' ), __( 'semanas', 'orosland' ) ),
        array( 60 * 60 * 24 , __( 'dia', 'orosland' ), __( 'dias', 'orosland' ) ),
        array( 60 * 60 , __( 'hora', 'orosland' ), __( 'horas', 'orosland' ) ),
        array( 60 , __( 'minuto', 'orosland' ), __( 'minutos', 'orosland' ) ),
        array( 1, __( 'segundo', 'orosland' ), __( 'segundos', 'orosland' ) )
    );
    if ( !is_numeric( $date ) ) {
        $time_chunks = explode( ':', str_replace( ' ', ':', $date ) );
        $date_chunks = explode( '-', str_replace( ' ', '-', $date ) );
        $date = gmmktime( (int)$time_chunks[1], (int)$time_chunks[2], (int)$time_chunks[3], (int)$date_chunks[1], (int)$date_chunks[2], (int)$date_chunks[0] );
    }
    $current_time = current_time( 'mysql', $gmt = 0 );
    $newer_date = time( );
    $since = $newer_date - $date;
    if ( 0 > $since )
        return __(  ' un momento', 'orosland' );
    for ( $i = 0, $j = count($chunks); $i < $j; $i++) {
        $seconds = $chunks[$i][0];
        if ( ( $count = floor($since / $seconds) ) != 0 )
            break;
    }
    $output = ( 1 == $count ) ? '1 '. $chunks[$i][1] : $count . ' ' . $chunks[$i][2];
    if ( !(int)trim($output) ){
        $output = '0 ' . __( 'segundos', 'orosland' );
    }
    return $output;
}

/* CUSTOM EXCERPT */
function get_excerpt($count){
    $foto = 0;
    $permalink = get_permalink($post->ID);
    $category = get_taxonomies($post->ID);
    $excerpt = get_the_excerpt($post->ID);
    if ($excerpt == ""){
        $excerpt = get_the_content($post->ID);
    }
    $excerpt = strip_tags($excerpt);
    $excerpt = substr($excerpt, 0, $count);
    $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
    $excerpt = $excerpt.'... <a class="plus" href="'.$permalink.'">+</a>';
    return $excerpt;
}

/* BREADCRUMBS */
function the_breadcrumb() {
    echo '<ol class="breadcrumb">';
    if (!is_home()) {
        echo '<li><a href="' . home_url('/') . '">'. __( 'Inicio', 'orosland' ) . '</a></li>';
        if (is_category() || is_single()) {
            echo '<li>' . get_the_category('title_li=') . '</li>';
            if (is_single()) {
                echo '<li class="active">' . get_the_title() . '</li>';
            }
        } elseif (is_page()) {
            echo '<li class="active">' . get_the_title() . '</li>';
        }
    }
    echo '</ol>';
}

/* IMAGES RESPONSIVE ON ATTACHMENT IMAGES */
function image_tag_class($class) {
    $class .= ' img-responsive';
    return $class;
}
add_filter('get_image_tag_class', 'image_tag_class' );

/* ADD CONTENT WIDTH FUNCTION */

if ( ! isset( $content_width ) ) $content_width = 1170;

function wp_42573_fix_template_caching( WP_Screen $current_screen ) {
    // Only flush the file cache with each request to post list table, edit post screen, or theme editor.
    if ( ! in_array( $current_screen->base, array( 'post', 'edit', 'theme-editor' ), true ) ) {
        return;
    }
    $theme = wp_get_theme();
    if ( ! $theme ) {
        return;
    }
    $cache_hash = md5( $theme->get_theme_root() . '/' . $theme->get_stylesheet() );
    $label = sanitize_key( 'files_' . $cache_hash . '-' . $theme->get( 'Version' ) );
    $transient_key = substr( $label, 0, 29 ) . md5( $label );
    delete_transient( $transient_key );
}
add_action( 'current_screen', 'wp_42573_fix_template_caching' );
