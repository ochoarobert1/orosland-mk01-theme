<footer class="container-fluid" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
    <div class="row">
        <div class="the-footer col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="footer-sub-container col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
                        <div class="footer-item col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <?php if ( is_active_sidebar( 'footer_sidebar' ) ) : ?>
                            <ul id="sidebar">
                                <?php dynamic_sidebar( 'footer_sidebar' ); ?>
                            </ul>
                            <?php endif; ?>
                        </div>
                        <div class="footer-item col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <?php if ( is_active_sidebar( 'footer_sidebar-2' ) ) : ?>
                            <ul id="sidebar">
                                <?php dynamic_sidebar( 'footer_sidebar-2' ); ?>
                            </ul>
                            <?php endif; ?>
                        </div>
                        <div class="footer-item col-lg-5 col-md-5 col-sm-5 col-xs-12">
                            <?php if ( is_active_sidebar( 'footer_sidebar-3' ) ) : ?>
                            <ul id="sidebar">
                                <?php dynamic_sidebar( 'footer_sidebar-3' ); ?>
                            </ul>
                            <?php endif; ?>
                            <div class="pre-header-social-icons footer-social-icons col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                <?php $link = get_option('orosland_fb'); ?>
                                <?php if ($link != '') { ?>
                                <a href="<?php echo esc_url($link); ?>" title="<?php _e('Visita nuestra página en Facebook', 'orosland'); ?>" target="_blank">
                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                </a>
                                <?php } ?>
                                <?php $link = get_option('orosland_tw'); ?>
                                <?php if ($link != '') { ?>
                                <a href="<?php echo esc_url($link); ?>" title="<?php _e('Visita nuestro perfil en Twitter', 'orosland'); ?>" target="_blank">
                                    <i class="fa fa-twitter" aria-hidden="true"></i>
                                </a>
                                <?php } ?>
                                <?php $link = get_option('orosland_gp'); ?>
                                <?php if ($link != '') { ?>
                                <a href="<?php echo esc_url($link); ?>" title="<?php _e('Visita nuestro perfil en Google Plus', 'orosland'); ?>" target="_blank">
                                    <i class="fa fa-google-plus" aria-hidden="true"></i>
                                </a>
                                <?php } ?>
                                <?php $link = get_option('orosland_yt'); ?>
                                <?php if ($link != '') { ?>
                                <a href="<?php echo esc_url($link); ?>" title="<?php _e('Visita nuestro canal en YouTube', 'orosland'); ?>" target="_blank">
                                    <i class="fa fa-youtube" aria-hidden="true"></i>
                                </a>
                                <?php } ?>
                                <?php $link = get_option('orosland_ps'); ?>
                                <?php if ($link != '') { ?>
                                <a href="<?php echo esc_url($link); ?>" title="<?php _e('Visita nuestra página en Pinterest', 'orosland'); ?>" target="_blank">
                                    <i class="fa fa-pinterest" aria-hidden="true"></i>
                                </a>
                                <?php } ?>
                                <?php $link = get_option('orosland_ig'); ?>
                                <?php if ($link != '') { ?>
                                <a href="<?php echo esc_url($link); ?>" title="<?php _e('Visita nuestra perfil en Instagram', 'orosland'); ?>" target="_blank">
                                    <i class="fa fa-instagram" aria-hidden="true"></i>
                                </a>
                                <?php } ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer() ?>
<!-- SITIO DESARROLLADO POR ROBERT OCHOA - http://robertochoa.com.ve/ -->
</body>
</html>
