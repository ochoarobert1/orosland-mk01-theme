<!DOCTYPE html>
<html <?php language_attributes() ?>>
    <head>
        <?php /* MAIN STUFF */ ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo('charset') ?>" />
        <meta name="robots" content="NOODP, INDEX, FOLLOW" />
        <meta name="HandheldFriendly" content="True" />
        <meta name="MobileOptimized" content="320" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="pingback" href="<?php echo esc_url(get_bloginfo('pingback_url')); ?>" />
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="dns-prefetch" href="//connect.facebook.net" />
        <link rel="dns-prefetch" href="//facebook.com" />
        <link rel="dns-prefetch" href="//googleads.g.doubleclick.net" />
        <link rel="dns-prefetch" href="//pagead2.googlesyndication.com" />
        <link rel="dns-prefetch" href="//google-analytics.com" />
        <?php /* FAVICONS */ ?>
        <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon.png" />
        <?php /* THEME NAVBAR COLOR */ ?>
        <meta name="msapplication-TileColor" content="#000000" />
        <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/images/win8-tile-icon.png" />
        <meta name="theme-color" content="#000000" />
        <?php /* AUTHOR INFORMATION */ ?>
        <meta name="language" content="<?php echo get_bloginfo('language'); ?>" />
        <meta name="author" content="Orosland" />
        <meta name="copyright" content="http://orosland.cl/" />
        <meta name="geo.position" content="-33.4727092,-70.7699158" />
        <meta name="ICBM" content="-33.4727092,-70.7699158" />
        <meta name="geo.region" content="CL" />
        <meta name="geo.placename" content="Santiago de Chile" />
        <meta name="DC.title" content="<?php if (is_home()) { echo get_bloginfo('name') . ' | ' . get_bloginfo('description'); } else { echo get_the_title() . ' | ' . get_bloginfo('name'); } ?>" />
        <?php /* MAIN TITLE - CALL HEADER MAIN FUNCTIONS */ ?>
        <?php wp_title('|', false, 'right'); ?>
        <?php wp_head() ?>
        <?php /* OPEN GRAPHS INFO - COMMENTS SCRIPTS */ ?>
        <?php get_template_part('includes/header-oginfo'); ?>
        <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
        <?php /* IE COMPATIBILITIES */ ?>
        <!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7" /><![endif]-->
        <!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8" /><![endif]-->
        <!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9" /><![endif]-->
        <!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js" /><!--<![endif]-->
        <!--[if IE]> <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script> <![endif]-->
        <!--[if IE]> <script type="text/javascript" src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script> <![endif]-->
        <!--[if IE]> <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" /> <![endif]-->
        <?php get_template_part('includes/fb-script'); ?>
        <?php get_template_part('includes/ga-script'); ?>
    </head>
    <body class="the-main-body <?php echo join(' ', get_body_class()); ?>" itemscope itemtype="http://schema.org/WebPage">
        <div id="fb-root"></div>
        <header class="container-fluid" role="banner" itemscope itemtype="http://schema.org/WPHeader">
            <div class="row">
                <div class="pre-header col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                    <div class="container">
                        <div class="row">
                            <div class="pre-header-social-icons col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <?php $link = get_option('orosland_fb'); ?>
                                <?php if ($link != '') { ?>
                                <a href="<?php echo esc_url($link); ?>" title="<?php _e('Visita nuestra página en Facebook', 'orosland'); ?>" target="_blank">
                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                </a>
                                <?php } ?>
                                <?php $link = get_option('orosland_tw'); ?>
                                <?php if ($link != '') { ?>
                                <a href="<?php echo esc_url($link); ?>" title="<?php _e('Visita nuestro perfil en Twitter', 'orosland'); ?>" target="_blank">
                                    <i class="fa fa-twitter" aria-hidden="true"></i>
                                </a>
                                <?php } ?>
                                <?php $link = get_option('orosland_gp'); ?>
                                <?php if ($link != '') { ?>
                                <a href="<?php echo esc_url($link); ?>" title="<?php _e('Visita nuestro perfil en Google Plus', 'orosland'); ?>" target="_blank">
                                    <i class="fa fa-google-plus" aria-hidden="true"></i>
                                </a>
                                <?php } ?>
                                <?php $link = get_option('orosland_yt'); ?>
                                <?php if ($link != '') { ?>
                                <a href="<?php echo esc_url($link); ?>" title="<?php _e('Visita nuestro canal en YouTube', 'orosland'); ?>" target="_blank">
                                    <i class="fa fa-youtube" aria-hidden="true"></i>
                                </a>
                                <?php } ?>
                                <?php $link = get_option('orosland_ps'); ?>
                                <?php if ($link != '') { ?>
                                <a href="<?php echo esc_url($link); ?>" title="<?php _e('Visita nuestra página en Pinterest', 'orosland'); ?>" target="_blank">
                                    <i class="fa fa-pinterest" aria-hidden="true"></i>
                                </a>
                                <?php } ?>
                                <?php $link = get_option('orosland_ig'); ?>
                                <?php if ($link != '') { ?>
                                <a href="<?php echo esc_url($link); ?>" title="<?php _e('Visita nuestra perfil en Instagram', 'orosland'); ?>" target="_blank">
                                    <i class="fa fa-instagram" aria-hidden="true"></i>
                                </a>
                                <?php } ?>

                            </div>
                            <div class="pre-header-social-buttons col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <button class="btn btn-md btn-pre-header">
                                    <a href="<?php echo home_url('/favoritos'); ?>" title="<?php _e('Ingresar al Carrito', 'orosland'); ?>">
                                        <i class="fa fa-heart"></i> <?php _e('Favoritos', 'orosland'); ?>
                                    </a>
                                </button>
                                <?php $cart_url = wc_get_cart_url(); ?>
                                <a href="<?php echo esc_url($cart_url); ?>" title="<?php _e('Ingresar al Carrito', 'orosland'); ?>">
                                    <button class="btn btn-md btn-pre-header">
                                        <i class="fa fa-shopping-cart"></i> <?php _e('Mi Carrito', 'orosland'); ?>
                                    </button>
                                </a>
                                <?php $myaccount_page_id = get_option( 'woocommerce_myaccount_page_id' ); ?>
                                <?php if ( $myaccount_page_id ) { $myaccount_page_url = get_permalink( $myaccount_page_id ); } ?>
                                <a href="<?php echo esc_url($myaccount_page_url); ?>" title="<?php _e('Ingresar a mi Cuenta', 'orosland'); ?>">
                                    <button class="btn btn-md btn-pre-header">
                                        <i class="fa fa-user-o"></i> <?php _e('Mi Cuenta', 'orosland'); ?>
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="the-logo col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                    <a href="<?php echo home_url('/'); ?>" title="<?php _e('Volver al Inicio', 'orosland'); ?>">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo-navbar.png" alt="<?php echo get_bloginfo('name'); ?>" class="img-responsive" />
                    </a>
                </div>
                <div id="sticker" class="the-header col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                    <nav class="navbar navbar-default" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
                        <div class="container-fluid">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>

                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <?php wp_nav_menu( array( 'theme_location' => 'header_menu', 'depth' => 2, 'container' => 'div',
                                                     'container_class' => 'collapse navbar-collapse', 'container_id' => 'bs-example-navbar-collapse-1', 'menu_class' => 'nav navbar-nav navbar-center', 'fallback_cb' => 'wp_bootstrap_navwalker::fallback', 'walker' => new wp_bootstrap_navwalker() )); ?>
                        </div><!-- /.container-fluid -->
                    </nav>
                </div>
            </div>
        </header>
